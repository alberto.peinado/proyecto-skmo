class ProyArray {

    constructor(){

        this.proyectos = [];
        this.lastId = 0;

    }

    addProyecto(p){

        p.id = this.lastId;
        this.proyectos.push(p);
        this.lastId = this.proyectos.length;
        
    }

}