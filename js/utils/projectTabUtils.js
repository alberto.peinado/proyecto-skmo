//GLOBALS

const deleteDialog = document.getElementById("deleteDialog");

/* FUNCIONALIDADES */

//Comprueba si la tab está activa

const isTabActive = tab => {

    return tab.classList.contains(activeClass)

}

//Desactiva la tab

const deactivateTab = tab => {

    tab.classList.remove(activeClass);

}

//- Activa la tab si no estaba activa ya y Desactiva la que lo estaba de antes

const activateTab = (tabList, tab) => {

    if (isTabActive(tab)) {

        return;

    }

    let found = false;

    for (let i = 0; i < tabList.length && !found; i++) {

        if (isTabActive(tabList[i])) {

            deactivateTab(tabList[i]);
            found = true;

        }

    }

    tab.classList.add(activeClass);
    activeTab = tab;

}

// - Doble check para el borrado

const confirmarBorrado = (event) => {

    if (confirm("El borrado será irreversible. ¿Desea continuar?")) {

        cleanAndClose();
        delProject(event.currentTarget.dataset);

    }

}

//- Pasa un objeto Proyecto a un elemento del DOM

const projectToDOM = project => {

    //Creación div.project-card

    const projectCard = document.createElement("div");
    projectCard.classList.add("project-card");

    //Creación div.project-card__top

    const projectTop = document.createElement("div");
    projectTop.classList.add("project-card__top");

    //Creación div.project-card__main

    const projectMain = document.createElement("div");
    projectMain.classList.add("project-card__main");
    projectMain.setAttribute("data-id", project.id);
    projectMain.onclick = (event) => {

        loadProject(event.currentTarget.dataset)

    }

    //Creación project-card__buttons

    const projectButtons = document.createElement("div");
    projectButtons.classList.add("project-card__buttons");

    //Creación project-card_btn(s) e iconos

    const projectBtnMod = document.createElement("div");
    projectBtnMod.classList.add("project-card__btn", "project-card__btn--mod");
    projectBtnMod.setAttribute("data-id", project.id);

    projectBtnMod.onclick = (event) => {

        loadProject(event.currentTarget.dataset, true);

    }

    const btnMod = document.createElement("i");
    btnMod.classList.add("icons", "icons--project", "fas", "fa-pencil-alt");

    const projectBtnDel = document.createElement("div");
    projectBtnDel.classList.add("project-card__btn", "project-card__btn--del");
    projectBtnDel.setAttribute("data-id", project.id);
    projectBtnDel.onclick = (event) => {

        $("#deleteDialog").dialog("open");
        $("#deleteDialog")[0].dataset.id = projectBtnDel.dataset.id;

    };

    const btnDel = document.createElement("i");
    btnDel.classList.add("icons", "icons--project", "fas", "fa-trash-alt");

    //Creación a.card-title.card-title--no-underline

    const cardTitle = document.createElement("a");
    cardTitle.classList.add("card-title", "card-title--no-underline");
    cardTitle.setAttribute("href", "#");
    cardTitle.innerHTML = project.title;

    //Creación div.project-card__text

    const cardText = document.createElement("div");
    cardText.classList.add("project-card__text");
    cardText.innerHTML = project.description;

    //Creación div.project-card__tag-bar

    const tagBar = document.createElement("div");
    tagBar.classList.add("project-card__tag-bar");

    projectTop.appendChild(projectMain);
    projectTop.appendChild(projectButtons);
    projectButtons.appendChild(projectBtnMod);
    projectBtnMod.appendChild(btnMod);
    projectButtons.appendChild(projectBtnDel);
    projectBtnDel.appendChild(btnDel);
    projectMain.appendChild(cardTitle);
    projectMain.appendChild(cardText);
    projectCard.appendChild(projectTop);
    projectCard.appendChild(tagBar);

    //Creación tags

    let extraLabels = 0;

    project.labels.forEach((lab, i) => {

        //Si hay más de dos tags, no crea elementos 
        //para ellas y aumenta el contador

        if (i < 2) {

            const tagDOM = document.createElement("div");
            tagDOM.classList.add("tag");
            tagDOM.innerHTML = lab;
            tagBar.appendChild(tagDOM);

        } else {

            extraLabels++;

        }

    });

    //Creación contador cuando hay más de 2 tags

    if (extraLabels) {

        const extraTag = document.createElement("div");
        extraTag.classList.add("tag", "tag--counter");
        extraTag.innerHTML = `+${extraLabels}`;
        tagBar.appendChild(extraTag);

    }

    return projectCard;

}

//- Pasa el Array de labels a un String separado por coma y espacio

const stringifyLabels = labArr => {

    let stringLabels = "";

    labArr.forEach(lab => stringLabels += `${lab}, `);

    stringLabels = stringLabels.slice(0, -2);

    return stringLabels;

}

//- Pasa un String de labels a un Array

const labelsToArray = labels => {

    const labArr = labels.split(",");

    labArr.forEach(lab => lab.trim());

    return labArr;

}

//- Adición de los proyectos al DOM

const displayProjects = (proyectos, targetType) => {

    const container = document.getElementById("project-cards-container");

    //Filtrado de proyectos por tipo

    let targetProjects;

    if (targetType !== "todos") {

        targetProjects = proyectos.filter(p => p.type === targetType);

    } else {

        targetProjects = proyectos;

    }

    let projectNodes = [];

    //Conversión de los proyectos a DOM

    targetProjects.forEach(p => projectNodes.push(projectToDOM(p)));

    //Vaciado y carga de los proyectos DOM

    container.innerHTML = "";

    projectNodes.forEach(p => container.appendChild(p));

}

//- Carga un proyecto a la card de detalle

const loadProject = (dataset, displaySave = false) => {

    const proyecto = proyArray.proyectos.find(p => p.id === parseInt(dataset.id));

    const title = document.querySelector("#titleField");
    const description = document.querySelector("#descriptionField");
    const labels = document.querySelector("#labelsField");
    const type = document.querySelector("#typeField");

    title.value = proyecto.title;
    description.value = proyecto.description;
    type.value = proyecto.type;
    labels.value = stringifyLabels(proyecto.labels);

    detalleCard.scrollIntoView();
    showDetalle(dataset.id, displaySave);

}

//- Borra un proyecto

const delProject = (dataset) => {

    //Index en el array del proyecto a borrar
    const targetIndex = proyArray.proyectos.findIndex(p => p.id === parseInt(dataset.id));

    proyArray.proyectos.splice(targetIndex, 1);

    displayProjects(proyArray.proyectos, activeTab.id);

}

//jQuery dialogs

$("#deleteDialog").dialog({
    autoOpen: false,
    dialogClass: 'deleteDialog',
    resizable: false,
    height: "auto",
    width: 300,
    modal: true,
    buttons: {
        "Aceptar": function () {
            $(this).dialog("close");
            delProject(this.dataset);
        },
        "Cancelar": function () {
            $(this).dialog("close");
        }
    }
});

$("#successDialog").dialog({
    
    autoOpen: false,
    dialogClass: 'successDialog',
    resizable: false,
    height: "auto",
    width: 300,
    modal: false,
    buttons: {
        "Cerrar": function () {
            $(this).dialog("close");
        }
    }

});

