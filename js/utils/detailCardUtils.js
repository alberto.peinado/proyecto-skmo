/*

    FUNCIONALIDADES DETAIL CARD

*/

//DOM elements

const detalleCard = document.getElementById("detalleCard");
const titleField = document.getElementById("titleField");
const descriptionField = document.getElementById("descriptionField");
const labelsField = document.getElementById("labelsField");
const typeField = document.getElementById("typeField");

//Botones

const plusBtn = document.getElementById("plusBtn");
const saveBtn = document.getElementById("saveBtn");
const exitBtn = document.getElementById("exitBtn");

let proyectoDetalle;

//Funciones

// - Muestra la card de detalle y añade atributos en función de CREATE (-1)/UPDATE(id)

const showDetalle = (id = -1, displaySave) => {

    saveBtn.setAttribute("data-id", -1);
    saveBtn.style.display = "none";

    if (id !== -1) {

        saveBtn.setAttribute("data-id", id);
        
    }
    
    if(displaySave){

        saveBtn.style.display = "block";

    }

    document.querySelector("#detalleCard").style.display = "block";
  
}

// - Valida que todos los campos estén rellenos, muestra el error si alguno no lo está

const validateFields = () => {

    let validData = true;

    titleField.nextElementSibling.style.display = 'none';
    descriptionField.nextElementSibling.style.display = 'none';
    labelsField.nextElementSibling.style.display = 'none';

    if (!titleField.value) {

        titleField.nextElementSibling.style.display = "block";
        validData = false;

    }

    if (!descriptionField.value && validData) {

        descriptionField.nextElementSibling.style.display = "block";
        validData = false;

    }

    if (!labelsField.value && validData) {

        labelsField.nextElementSibling.style.display = "block";
        validData = false;

    }

    return validData;

}

// - Limpia los avisos de error que pudiera haber y vacía los inputs

const cleanFieldset = () => {

    titleField.nextElementSibling.style.display = 'none';
    descriptionField.nextElementSibling.style.display = 'none';
    labelsField.nextElementSibling.style.display = 'none';

    titleField.value = "";
    descriptionField.value = "";
    labelsField.value = "";

}

const cleanAndClose = () => {

    detalleCard.style.display = 'none';
    saveBtn.style.display = 'none';
    cleanFieldset();

}

// - Guarda el proyecto si los datos son válidos

const saveProject = () => {

    //Comprobación de inputs válidos

    const validData = validateFields();

    if (!validData) {

        return;

    }

    proyectoDetalle = new Proyecto(

        titleField.value,
        descriptionField.value,
        labelsToArray(labelsField.value),
        typeField.value

    );

    cleanAndClose();
    $("#successDialog").dialog("open");

    proyArray.addProyecto(proyectoDetalle);

    //Si hemos creado un proyecto de otra categoría
    //a la que está seleccionada, cambiamos de tab
    //y mostramos el proyecto nuevo

    if (activeTab.id !== proyectoDetalle.type) {

        const targetTab = document.querySelector(`#${proyectoDetalle.type}`);
        activateTab(tabsProyectos, targetTab);

    }

    displayProjects(proyArray.proyectos, activeTab.id);

}

// - Actualiza un proyecto si los datos son válidos

const updateProject = id => {

    //Valida datos

    const validData = validateFields();

    if (!validData) {

        return;

    }

    const targetIndex = proyArray.proyectos.findIndex(p => p.id === parseInt(id));

    proyArray.proyectos[targetIndex].title = titleField.value;
    proyArray.proyectos[targetIndex].description = descriptionField.value;
    proyArray.proyectos[targetIndex].labels = labelsToArray(labelsField.value);
    proyArray.proyectos[targetIndex].type = typeField.value;

    cleanAndClose();
    $("#successDialog").dialog("open");

    //Si hemos actualizado el proyecto a otra categoría
    //de la que está seleccionada, cambiamos de tab
    //y mostramos el proyecto nuevo

    if (activeTab.id !== proyArray.proyectos[targetIndex].type) {

        const targetTab = document.querySelector(`#${proyArray.proyectos[targetIndex].type}`);
        activateTab(tabsProyectos, targetTab);

    }

    displayProjects(proyArray.proyectos, activeTab.id);

}

// EVENTS

plusBtn.onclick = () => {

    showDetalle(-1, true);

}

saveBtn.onclick = (event) => {

    if (parseInt(event.target.dataset.id) === -1) {

        saveProject();
        
    } else {
        
        updateProject(event.target.dataset.id);

    }

}

exitBtn.onclick = cleanAndClose;