//Creación instancias de Proyectos e inclusión en Array

const p1 = new Proyecto("SKM: intranet inteligente", "Proyecto interno", ['Intranet', 'Buscador inteligente', 'SKM'], 'enCurso');
const p2 = new Proyecto("Front Academy: formación interna", "Proyecto de formación interno", ['Formación', 'Front-end', 'Becarios', 'Vue'], 'enCurso');
const p3 = new Proyecto("Semilla sass: migración versiones", "Actualización semilla", ['DFront'], 'cerrados');
const p4 = new Proyecto("SKM: intranet inteligente", "Proyecto interno", ['Intranet', 'Buscador inteligente', 'SKM'], 'relacionados');
const p5 = new Proyecto("Lorem: ipsum inteligente", "Proyecto latino", ['Lorem', 'Fistrum', 'SKM', 'Oueue', 'sss'], 'relacionados');

//Globales

const activeClass = "tabs-bar__tab--active";
const dropdownMenu = document.querySelector("#dropdownMenu");

let activeTab = document.querySelector(`.${activeClass}`);

const tabsProyectos = document.getElementById("tabs-bar-projects").children;

const proyArray = new ProyArray();

proyArray.addProyecto(p1);
proyArray.addProyecto(p2);
proyArray.addProyecto(p3);
proyArray.addProyecto(p4);
proyArray.addProyecto(p5);

//Añadido de eventListeners

for(let i = 0; i < tabsProyectos.length; i++){

    const thisTab = tabsProyectos[i];

    tabsProyectos[i].addEventListener("click", () => {

        //Cambio de clases en HTML
        activateTab(tabsProyectos, thisTab)

        //Display de proyectos para esa tab
        displayProjects(proyArray.proyectos, thisTab.id);

    });

}

//Centrado del dialog cuando está abierto

window.onresize = () => {

    const sucDialog = $(".successDialog");
    const delDialog = $(".deleteDialog");

    if (sucDialog.css("display") === "block") {

        const leftValue = (window.innerWidth - parseInt(sucDialog.css("width"))) / 2;
        sucDialog.css("left", leftValue);

    } else if (delDialog.css("display") === "block") {

        const leftValue = (window.innerWidth - parseInt(delDialog.css("width"))) / 2;
        delDialog.css("left", leftValue);

    }
    
    if(window.innerWidth > 1023 && dropdownMenu.classList.contains("show")){

        dropdownMenu.classList.remove("show");

    }

}

displayProjects(proyArray.proyectos, activeTab.id);
